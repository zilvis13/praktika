<?php

namespace App\Http\Controllers;
use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FrontController extends Controller
{
    public function index()
    {
        // $produktai=Product::all();
        // return view('front.home',compact('produktai'));
        $produktai = DB::table('products')
        ->offset(0)
        ->limit(4)
        ->orderBy('created_at', 'desc')
        ->get();
return view('front.home',compact('produktai'));
    }

    public function produktai()
    {
        $produktai=Product::all();
        return view('front.produktai', compact('produktai'));
    }

    public function produktas(Product $product)
    {
        $produktai=Product::all();
        return view('front.produktas',compact('product'));
    }

    public function kategorijos()
    {
        $categories=Category::all();
        return view('front.kategorijos',compact('categories'));
    }

    public function edit($id)
    {
        $product=Product::find($id);
        return view('productupdateform')->with('product',$product);
    }
}

