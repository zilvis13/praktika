@extends('layout.main')
@section('title', 'Produktas')
@section('content')
        <div class="row">
            <div class="small-5 small-offset-1 columns">
                <div class="item-wrapper">
                    <div class="img-wrapper">
                        <a href="#">
                        <img src="{{asset("storage/$product->image")}}"/>
                        </a>
                    </div>
                </div>
            </div>
           )
            <div class="small-6 columns">
                <div class="item-wrapper">
                    <h3 class="subheader">
                       <span class="price-tag">${{$product->price}}</span>
                    </h3>
                    <div class="row">
                        <div class="large-12 columns">
                        
                            <label>
                                Pasirinkite
                                <select>
                                    <option value="small">
                                        Mazas
                                    </option>
                                    <option value="medium">
                                        Vidutinis
                                    </option>
                                    <option value="large">
                                        Didelis
                                    </option>

                                    <p>{!! $product->description !!}
                                    </p>
                                   
                                </select>
                            </label>
                            <a href="#" class="button  expanded">Prideti i krepseli</a>
                        </div>
                    </div>
                <p class="text-left subheader"><small>*Būtinai pasirinkite dydį</small></p>

                </div>
            </div>
        </div>
    </div>
</body>
</html>
@endsection