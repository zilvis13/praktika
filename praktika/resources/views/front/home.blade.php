@extends('layout.main')

@section('content')
<section class="hero text-center">
            <br/>
            <br/>
            <br/>
            <br/>
            <h2 >
                <strong>
                Sveiki atvyke!
                </strong>
            </h2>
        </section>
        <br/>
        <div class="subheader text-center">
             <h2>
             Naujausi musu produktai
        </h2>
        </div>
       
        
        <div class="row">
            @forelse($produktai as $produktas)
            <div class="small-3 columns">
                <div class="item-wrapper">
                    <div class="img-wrapper">
                        <a href="{{route('cart.edit', $produktas->id)}}" class="button expanded add-to-cart">
                            Prideti
                        </a>
                        <a href="#">
                            <img src="{{url('images', $produktas->image)}}"/>
                        </a>
                    </div>
                    <a href="{{url('/produktas')}}">
                        <h3>
                        {{$produktas->name}}
                        </h3>
                    </a>
                    <h5>
                    {{$produktas->price}}
                    </h5>
                    <p>
                    {{$produktas->size}}
                    </p>
                    <p>
                    {{$produktas->description}}
                    </p>
                </div>
            </div>
            @empty
        <h4>Nera produktu</h4>
            @endforelse
        </div>
        
        <br>
@endsection