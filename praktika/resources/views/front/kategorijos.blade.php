@extends('layout.main')
@section('title', 'Kategorijos')
@section('content')


    <div class="navbar">
        <a class="navbar-brand" href="#">Kategorijos</a>
        <ul class="nav navbar-nav">
            @if(!empty($categories))
            @forelse($categories as $category)
                <li>
                    <a href="{{route('category.show', $category->id)}}">{{$category->name}}</a>
                </li>
            @empty
            <li>Nera duomenu</li>
            @endforelse
        @endif

        </ul>

            </div>
        </div>


    @if(!empty($products))
    <h3>Products</h3>

    <table class="table table-hover">
    	<thead>
    		<tr>
    			<th>Products</th>
    		</tr>
    	</thead>
    	<tbody>
@forelse($products as $product)
    <tr><td>{{$product->name}}</td></tr>
    	@empty
        <tr><td>Nera produktu</td></tr>
        @endforelse

        </tbody>
    </table>
    @endif

@endsection