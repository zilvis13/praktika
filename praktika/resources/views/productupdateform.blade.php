@extends('admin.layout.admin')

@section('content')

{!! Form::open(array('route' => array('product.update', $product->id))) !!}
        @method('PATCH')
        {!! Form::token() !!}
        <div class="form-group">
        {{ Form::label('name','Pavadinimas') }}
        {{ Form::text('name', $product->name, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('description','Aprasymas') }}
        {{ Form::text('description', $product->description, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('price', 'Kaina') }}
        {{ Form::text('price', $product->price, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('size','Dydis') }}
        {{ Form::select('size',['mazas' => 'mazas', 'vidutinis' => 'Vidutinis', 'didelis'=>'Didelis'], $product->size, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('image','Nuotrauka') }}
        {{ Form::file('image', array('class' => 'form-control')) }}
    </div>
    {!! Form::submit('Saugoti', array('class' => 'btn btn-primary')) !!} 
    {{ Form::close() }}
@endsection