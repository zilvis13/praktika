@extends('layout.main')
@section('content')

<h3>Krepselis</h3>


<table class="table table-hover">
    <thead>
        <tr>
            
            <th>Pavadinimas</th>
            <th>Kaina</th>
            <th>Kiekis</th>
            <th>Veiksmas</th>
            
        </tr>
    </thead>
    <tbody>
    @foreach($cartItems as $cartItem)
        <tr>
            <td>{{$cartItem->image}}</td>
            <td>{{$cartItem->name}}</td>
            <td>{{$cartItem->price}}</td>
            <td width="100px">
            {!! Form::open(['route' => ['cart.update',$cartItem->rowId], 'method' => 'put']) !!}
            <input name="qty" type="number" value="{{$cartItem->qty}}">
            <input type="submit" class="btn btn-sm btn-default" value="Ok">
            {!! Form::close() !!}
            </td>
            
            
            <td>{{$cartItem->options->has('size')?$cartItem->options->size:''}}</td>

            <td>
                <form action="{{route('cart.destroy',$cartItem->rowId)}}"  method="POST">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <input class="button small alert" type="submit" value="Trinti">
                    </form>
            </td>


        </tr>
        @endforeach
        <tr>
            <td></td>
            <td>Viso moketi: {{Cart::subtotal()}}$</td>
            <td>Kiekis: {{Cart::count()}}</td>
        </tr>
    </tbody>
</table>
@endsection