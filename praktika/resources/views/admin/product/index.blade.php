@extends('admin.layout.admin')

@section('content')

<h3>Produktai</h3>
<ul>
    @forelse($products as $product)
    <li>
        <h4>ID#{{$product->id}} {{$product->name}}</h4>
        <img src="{{url('images', $product->image)}}" width="150px"/>
        <br/>
        <a href="edit/{{ $product->id }}" class="btn btn-success">Redaguoti</a>
        <form action="{{ route('product.destroy', $product->id)}}" method="post">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger btn-sm" type="submit">Trinti</button>
        </form>
    
        </li>
    @empty

    <h3>Nera produktu</h4>
    
    @endforelse

</ul>
@endsection

