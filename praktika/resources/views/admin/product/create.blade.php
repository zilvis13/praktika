@extends('admin.layout.admin')

@section('content')

<h3>Prideti nauja produkta</h3>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">

    {!! Form::open(['route' => 'product.store', 'method' => 'POST', 'files' => true]) !!}

    <div class="form-group">
        {{ Form::label('name','Pavadinimas') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('description','Aprasymas') }}
        {{ Form::text('description', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('price', 'Kaina') }}
        {{ Form::text('price', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('size','Dydis') }}
        {{ Form::select('size', ['mazas' => 'mazas', 'vidutinis' => 'Vidutinis', 'didelis'=>'Didelis'], null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('category_id','Kategorija') }}
        {{ Form::select('category_id',$categories , null, ['class' => 'form-control','placeholder'=>'Pasirinkite Kategorija']) }}
    </div>

    <div class="form-group">
        {{ Form::label('image','Nuotrauka') }}
        {{ Form::file('image', array('class' => 'form-control')) }}
    </div>

    
    {{ Form::submit('Prideti', array('class' => 'btn btn-default')) }}
       
    

    {!! Form::close() !!}
        </div>
    </div>
@endsection

