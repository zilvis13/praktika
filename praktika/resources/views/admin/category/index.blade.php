@extends('admin.layout.admin')

@section('content')

    <div class="navbar">
        <a class="navbar-brand" href="#">Kategorijos</a>
        <ul class="nav navbar-nav">
            @if(!empty($categories))
            @forelse($categories as $category)
                <li>
                    <a href="{{route('category.show', $category->id)}}">{{$category->name}}</a>
                </li>
            @empty
            <li>Nera duomenu</li>
            @endforelse
        @endif

        </ul>

        <a class="btn btn-primary pull-right navbar-right" data-toggle="modal" href="#category">Prideti</a>
    <div class="modal fade" id="category">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Prideti kategorija</h4>
                </div>
                {!! Form::open(['route' => 'category.store', 'method' => 'post']) !!}
                <div class="modal-body">
                    <div class="form-group">
                        {{ Form::label('name', 'Pavadinimas') }}
                        {{ Form::text('name', null, array('class' => 'form-control')) }}

                    </div>
                    

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Uzdaryti</button>
                    <button type="submit" class="btn btn-primary">Isaugoti</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    </div>
    </div>

    @if(!empty($products))
    <h3>Produktai</h3>

    <table class="table table-hover">
    	<thead>
    		<tr>
    			<th>Visi produktai</th>
    		</tr>
    	</thead>
    	<tbody>
@forelse($products as $product)
    <tr><td>
    #{{$product->id}}
    {{$product->name}}
    <img src="{{url('images', $product->image)}}" width="150px"/>
    
    
    </td></tr>
    	@empty
        <tr><td>Nera produktu</td></tr>
        @endforelse

        </tbody>
    </table>
    @endif

@endsection