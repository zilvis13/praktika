{{-- Side Navigation --}}
<div class="col-md-2">
    <div class="sidebar content-box" style="display: block;">
        <ul class="nav">
            
            <li class="current"><a href="{{route('admin.index')}}"><i class="glyphicon glyphicon-home"></i>
                    Valdymas</a></li>
            <li class="submenu">
                <a href="#">
                    <i class="glyphicon glyphicon-list"></i> Produktai
                    <span class="caret pull-right"></span>
                </a>
                
                <ul>
                    <li><a href="{{route('product.index')}}">Produktai</a></li>
                    <li><a href="{{route('product.create')}}">Prideti produkta</a></li>
                </ul>
            </li>
            <li class="submenu">
                <a href="#">
                    <i class="glyphicon glyphicon-list"></i> Kategorijos
                    <span class="caret pull-right"></span>
                </a>
                
                <ul>
                    <li><a href="{{route('category.index')}}">Kategorijos</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div> 