<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'FrontController@index')->name('home');
Route::get('produktai', 'FrontController@produktai');
Route::get('produktas', 'FrontController@produktas');
Route::get('kategorijos', 'FrontController@kategorijos');
Auth::routes();
Route::get('/logout', 'auth\LoginController@logout');

Route::get('admin/edit/{id}', 'FrontController@edit');

Route::get('/home', 'FrontController@index');
Route::resource('/cart', 'CartController');

Route::group(['prefix' => 'admin', 'middleware' =>'auth'], function() {
    Route::get('/', function () {
        return view('admin.index');
    })->name('admin.index');

    Route::resource('product', 'ProductsController');
    Route::resource('category', 'CategoriesController');
});

?>